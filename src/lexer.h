#ifndef LEXER_H_
#define LEXER_H_

#define TKX_EOF -1

struct lexer_t;

struct lexer_t *lexer_create(void);
void lexer_destroy(struct lexer_t *lex);
void lexer_add_buffer(struct lexer_t *lex, const char *buffer);
int lexer_get_token(struct lexer_t *lex);

#endif
