#include <src/lexer.h>
#include <stdio.h>
#include <src/parser.h>
#include <stdlib.h>

struct lexer_t {
	const char *buffer;
	size_t ptr;
};

struct lexer_t *lexer_create(void) {
	return (struct lexer_t *) calloc(1, sizeof(struct lexer_t));
}

void lexer_destroy(struct lexer_t *lex) {
	free(lex);
}

void lexer_add_buffer(struct lexer_t *lex, const char *buffer) {
	lex->buffer = buffer;
	lex->ptr = 0;
}

static size_t lexer_get_token_length(struct lexer_t *lex, int *token_type) {
	char c;
	size_t length;

	*token_type = 0;

	c = lex->buffer[lex->ptr];
	while (c == ' ' || c == '\t') {
		lex->ptr++;
		c = lex->buffer[lex->ptr];
	}

	switch (c) {
	case '\0':
		*token_type = TKX_EOF;
		return 0;
	case '{':
		*token_type = TK_L_BRACE;
		return 1;
	case '}':
		*token_type = TK_R_BRACE;
		return 1;
	case '=':
		*token_type = TK_EQ;
		return 1;
	}

	if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')) {
		length = 1;
		c = lex->buffer[lex->ptr + length];
		while (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')
				|| ('0' <= c && c <= '9')) {
			length++;
			c = lex->buffer[lex->ptr + length];
		}
		*token_type = TK_ID;
		return length;
	}

	if (c == '"') {
		length = 1;
		c = lex->buffer[lex->ptr + length];
		while (c != '"' && c != '\0') {
			length++;
			c = lex->buffer[lex->ptr + length];
			if (c == '\\') {
				length++;
				c = lex->buffer[lex->ptr + length];
				if (c != '\0') {
					length++;
					c = lex->buffer[lex->ptr + length];
				}
			}
		}
		if (c == '"') {
			length++;
		}
		*token_type = TK_STRING;
		return length;
	}

	return 0;
}

int lexer_get_token(struct lexer_t *lex) {
	int tok_type = 0;
	size_t tok_length = 0;

	tok_length = lexer_get_token_length(lex, &tok_type);
	lex->ptr += tok_length;

	return tok_type;
}
