#include <check.h>
#include <src/lexer.c>

START_TEST( construct_destruct) {
	struct lexer_t *lex;
	ck_assert((lex = lexer_create()) != NULL);
	lexer_destroy(lex);
}
END_TEST

START_TEST( accept_buffer) {
	struct lexer_t *lex;
	int tok;
	ck_assert((lex = lexer_create()) != NULL);

	lexer_add_buffer(lex, "ds Badboll \"boll\\\" tjipp\\a\" = { kaka }");
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TK_ID);
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TK_ID);
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TK_STRING);
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TK_EQ);
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TK_L_BRACE);
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TK_ID);
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TK_R_BRACE);
	tok = lexer_get_token(lex); ck_assert_int_eq(tok, TKX_EOF);
	lexer_destroy(lex);
}
END_TEST

Suite *lexer_suite(void) {
	Suite *s = suite_create("Lexer");

	TCase *tc = tcase_create("Lexer testcase");
	tcase_add_test(tc, construct_destruct);
	tcase_add_test(tc, accept_buffer);
	suite_add_tcase(s, tc);

	return s;
}

int main(void) {
	int number_failed;
	Suite *s = lexer_suite();
	SRunner *sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
